#include "lib/deviceConfig.h"
//#include <math.h>
#Include "lib/1wire.c"
#Include "lib/ds18b20.c"
//************************************************************************************************************************************* 
//--------------------------------------Funciones globales
//*************************************************************************************************************************************
float getTempOperacion(void);
float getTempConf(void);
float getTempSelected(void);
int16 ReadSensor(int chanel);
void titilo(int cant);
void timer1_config_delay(int16 a);
//************************************************************************************************************************************* 
//--------------------------------------Variables globales
//*************************************************************************************************************************************

volatile int8 acumu1=0;
int1 enciende=0;

int1 standbyMode=1;
const int MUESTRA = 5;
const float TEMCRI = 2.0;
volatile float tempMem=0.1, temperatureOp=0,temperatureAlarm=0,tempSelect1=0,tempSelect2=0,tempSelect3=0;
struct{
    INT16 Count;
    INT16 CountMax;
    INT1 TimeOut;
}Temporizador;
//************************************************************************************************************************************* 
//--------------------------------------Interrupcion Timer1
//*************************************************************************************************************************************
#int_TIMER1
void TIMER1_isr(void)
{ 
    if((++Temporizador.Count)==Temporizador.CountMax) //3 min CountMax 420 
    {
        Temporizador.TimeOut=1;
    }
    set_timer1(0);//0.262 seg OCS8MHz
}
//************************************************************************************************************************************* 
//--------------------------------------Programa Principal
//*************************************************************************************************************************************
void main()
{

#asm //codigo en Assembler para poner los pines rc en digital

CLRF LATC                           ; Initialize output data; latch values for logic
                                    ; output low value.
MOVLB 0x0F                          ; ANCONx registers are
                                    ; not in access bank
                                    ;Configure RC2/AN11 for digital input mode
BSF ANCON1, 3                       ;Disable USB transceiver to use RC4/RC5 as
                                    ;general purpose inputs
BCF UCON, 3                         ;Disable USB module
BSF UCFG, 3                         ;Disable USB transceiver
MOVLW 0x03                          ; 0x03-> RC0 configured as digital input 1
MOVWF TRISC                         ;        RC1 configured as digital input 1
                                    ;        RC2 configured as digital output 0
                                    ;        RC3 configured as digital output 0
                                    ;        RC4 configured as digital output 0
                                    ;        RC5 configured as digital output 0
                                    ;        RC6 configured as digital output 0
                                    ;        RC7 configured as digital output 0
#endasm //salimos del bloque assembler
   //configuro el oscilador
   //INTSRC PLLEN TUN5 TUN4 TUN3 TUN2 TUN1 TUN0
   //INTSRC : 0 = 31 kHz device clock derived directly from INTRC internal oscillator
   OSCTUNE =  0b00000000 ; //Selecciono el oscilador de baja frecuencia y la activacion del PLL
   //IDLEN IRCF2 IRCF1 IRCF0 OSTS � SCS1 SCS0
   OSCCON = 0b11111011 ; //11110000
   //setup_oscillator(OSC_PLL_OFF | OSC_8MHZ | OSC_INTRC); //Configuro osc interno a 8mhz
   //End configuro el oscilador
   /*
     * Configuramos los puetos como digitales y analogicos.
   */
   //ANCON0: A/D PORT CONFIGURATION REGISTER 2 (BANKED F48h)
   ANCON0 = 0b11111101; //ra1 an1 analog
   ANCON1 = 0b10011111; //all digital
   
   setup_adc_ports(sAN1 | VSS_VDD); //PARA LEER EL POTENCIOMETRO AN1 RA1
   
   setup_adc(ADC_CLOCK_INTERNAL | ADC_TAD_MUL_0); 
   disable_interrupts(INT_TIMER1);//desactiva interrupcion de timer 1
   timer1_config_delay(420);// 687 cortes = 3min
   set_tris_B(0b00000000); //todos output
   set_tris_C(0b00000011); //rc0 y rc1 input
   //set_tris_A(0b00000110);//ra4 no esta implementado
   
   output_low(outSolenoide);//apago el Solenoide
   output_low(outCompresor);//apago el compresor5
   titilo(3);
   output_low(ledAlarma);//apago el ledAlarma
   ds18b20_init(1); //inicio senTempOperacion
   ds18b20_init(2);//inicio senTempCongelacion
   tempSelect3 = getTempSelected();
   temperatureOp = getTempOperacion();
   temperatureAlarm = getTempConf();
   
   while(TRUE)
   {
     //TODO: User Code
     //control input bomba y flujo. CERRADO el circuito
      if (input(inBomFlow)==0){
               temperatureAlarm = getTempConf();
               delay_ms(50);
               if (temperatureAlarm < TEMCRI ){
                    output_high(ledAlarma);//enciendo el compresor1
                    acumu1=0;
                    enciende=0; //apago el sistema
                    standbyMode=1; //prendo el standby
               }else{
                    output_low(ledAlarma);//enciendo el compresor1
                    standbyMode=0; //apago el standby
               }
                     

     }

     if (input(inBomFlow)==1){//control input bomba y flujo. ABIERTO el circuito
         delay_ms(50);
         if (input(inBomFlow)==1){
             standbyMode=1; //prendo el standby
             acumu1=0;
             enciende=0; //apago el sistema
             output_low(ledAlarma);//enciendo el compresor1
         }
     }
     
    if(standbyMode==0){   
               
               //cada 3 min entra
               if (Temporizador.TimeOut==1)
               {
                   Temporizador.TimeOut=0;
                   Temporizador.Count=0;
                   //action:
                      if(enciende==0){
                            //arranca por primera vez. 
                            acumu1=2;//al pasar los 3 minutos activo los 1 acumuladores
                            enciende=1;
      
                      }else{ //si ya esta prendido
                           
                           if(acumu1<2){ 
                                 acumu1++;
                           }
                      } 
                   
               }
               if(enciende){
                       tempSelect3 = getTempSelected();
                       temperatureOp = getTempOperacion();
                       
                       if(tempSelect3 != tempMem){
                          tempMem=tempSelect3;
                          tempSelect1 = (tempSelect3 - 0.5);
                          tempSelect2 = (tempSelect3 + 0.5);
                       }
                       
                       if (tempSelect2 <= temperatureOp ){
                          delay_ms(1000);
                          output_high(outSolenoide);//enciendo el solenoide dejo circular el gas              
                       }else if ( temperatureOp <= tempSelect1 ){
                          delay_ms(1000);
                          output_low(outSolenoide);//apago el compresor
                       }else{
                            //non
                       }
                       //control input presostato ABIERTO.
                       if (input(inPresostato)==0){
                           delay_ms(50);
                           if (input(inPresostato)==0){
                                    if(acumu1==2){ //comprueba que el acumulardor este en 3min
                                        output_high(outCompresor);//enciendo el compresor
                                        delay_ms(9000);
                                    }
                           }
                       }else{ //presostato cerrado
                           delay_ms(50);
                           if (input(inPresostato)==1){
                                acumu1=0;
                                output_low(outCompresor);//apago el compresor
                                delay_ms(9000);
                           }
                       }
                         
               }else{
                  output_low(outCompresor);//apago el compresor1
                  output_low(outSolenoide);//apago el compresor5
               }//fin enciende.
     }else{
            output_low(outCompresor);//apago el compresor1
            output_low(outSolenoide);//apago el compresor5
     }//fin standBy.
   }

}
//************************************************************************************************************************************* 
//--------------------------------------Funciones Implementadas
//*************************************************************************************************************************************
float getTempOperacion(void)
{  
   float tempe1=0;
   for (int i=0 ;i<MUESTRA;++i){
      tempe1 += ds18b20_read(1);
   }
   tempe1 /= MUESTRA;
   if (tempe1<0){
    tempe1 = temperatureOp;
   }
   return tempe1;
}
float getTempConf(void)
{  
   float tempe2=0;
   for (int i=0 ;i<MUESTRA;++i){
      tempe2 += ds18b20_read(2);
   }
   tempe2 /= MUESTRA;
   return tempe2;
}

float getTempSelected(void)
{
   float tempSel=0;
   tempSel= (0.0098 * ReadSensor(1)) + 5 ; //lee el puerto AN1 RA1
   return tempSel;
}
int16 ReadSensor(int chanel)
{
   int16 digi=0;
   for(int d=0; d<10; d++)
   {
      set_adc_channel(chanel); //the next read_adc call will read channel 0
      delay_ms(1); //a small delay is required after setting the channel and before read
      read_adc(ADC_START_ONLY); //only starts the conversion
      while (adc_done()==False);
      digi+=read_adc();
   }
   if(digi<=0){
      digi=1;
   }else{
      digi/=10;
   }
   return(digi);
}
void timer1_config_delay(int16 a) //3min=180seg = 687 cortes
{
  //PIN_ON(LED2);
  Temporizador.Count=0;
  Temporizador.CountMax=a;
  Temporizador.TimeOut=0;
  setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);      //262 ms overflow
  set_timer1(0); //esto nos da 0.262 seg OCS8MHz
  enable_interrupts(INT_TIMER1); 
  enable_interrupts(GLOBAL); 
}  

void titilo(int cant)
{
   for(int l=0; l<cant ; l++){
      output_high(ledAlarma);
      delay_ms(100);
      output_low(ledAlarma);
      delay_ms(100);
      output_high(ledAlarma);
      delay_ms(100);
      output_low(ledAlarma);
   }
   return;
}


