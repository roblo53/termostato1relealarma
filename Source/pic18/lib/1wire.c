/***********************1Wire Class***********************/ 
/*Description: This class handles all communication */ 
/* between the processor and the 1wire */ 
/* sensors. 
/*********************************************************/ 

/*-------1-wire definitions-------*/ 
#define ONE_WIRE_PIN1 senTempOper
#define ONE_WIRE_PIN2 senTempConf
/*******************1-wire communication functions********************/ 


void onewire_reset(Int8 NumSen)  // OK if just using a single permanently connected device 
{
   if(NumSen==1){
      output_low(ONE_WIRE_PIN1); 
      delay_us( 500 ); // pull 1-wire low for reset pulse 
      output_float(ONE_WIRE_PIN1); // float 1-wire high 
      delay_us( 500 ); // wait-out remaining initialisation window. 
      output_float(ONE_WIRE_PIN1);
   }
   if(NumSen==2){
      output_low(ONE_WIRE_PIN2); 
      delay_us( 500 ); // pull 1-wire low for reset pulse 
      output_float(ONE_WIRE_PIN2); // float 1-wire high 
      delay_us( 500 ); // wait-out remaining initialisation window. 
      output_float(ONE_WIRE_PIN2);
   }
} 

/*********************** onewire_write() ********************************/ 
/*This function writes a byte to the sensor.*/ 
/* */ 
/*Parameters: byte - the byte to be written to the 1-wire */ 
/*Returns: */ 
/*********************************************************************/ 

void onewire_write(int8 data,NumSen) 
{ 
   int count; 
   if(NumSen==1){
         for (count=0; count<8; ++count) 
         { 
           output_low(ONE_WIRE_PIN1); 
           delay_us( 2 ); // pull 1-wire low to initiate write time-slot. 
           output_bit(ONE_WIRE_PIN1, shift_right(&data,1,0)); // set output bit on 1-wire 
           delay_us( 60 ); // wait until end of write slot. 
           output_float(ONE_WIRE_PIN1); // set 1-wire high again, 
           delay_us( 2 ); // for more than 1us minimum. 
         } 
   }
   if(NumSen==2){
         for (count=0; count<8; ++count) 
         { 
           output_low(ONE_WIRE_PIN2); 
           delay_us( 2 ); // pull 1-wire low to initiate write time-slot. 
           output_bit(ONE_WIRE_PIN2, shift_right(&data,1,0)); // set output bit on 1-wire 
           delay_us( 60 ); // wait until end of write slot. 
           output_float(ONE_WIRE_PIN2); // set 1-wire high again, 
           delay_us( 2 ); // for more than 1us minimum. 
         } 
   }
} 

/*********************** read1wire() *********************************/ 
/*This function reads the 8 -bit data via the 1-wire sensor. */ 
/* */ 
/*Parameters: */ 
/*Returns: 8-bit (1-byte) data from sensor */ 
/*********************************************************************/ 

int onewire_read(int8 NumSen) 
{ 
   int count, data;
   if(NumSen==1)
   {
      for (count=0; count<8; ++count) 
      { 
        output_low(ONE_WIRE_PIN1); 
        delay_us( 2 ); // pull 1-wire low to initiate read time-slot. 
        output_float(ONE_WIRE_PIN1); // now let 1-wire float high, 
        delay_us( 8 ); // let device state stabilise, 
        shift_right(&data,1,input(ONE_WIRE_PIN1)); // and load result. 
        delay_us( 120 ); // wait until end of read slot. 
      }
   }
   if(NumSen==2)
   {
      for (count=0; count<8; ++count) 
      { 
        output_low(ONE_WIRE_PIN2); 
        delay_us( 2 ); // pull 1-wire low to initiate read time-slot. 
        output_float(ONE_WIRE_PIN2); // now let 1-wire float high, 
        delay_us( 8 ); // let device state stabilise, 
        shift_right(&data,1,input(ONE_WIRE_PIN2)); // and load result. 
        delay_us( 120 ); // wait until end of read slot. 
      }
   }
   return( data ); 
}
