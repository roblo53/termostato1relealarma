void ds18b20_init(int8 NumSen) 
{ 
//for  10 bit resolution mod  
   onewire_write(0xCC,NumSen); 
   onewire_write(0x4E,NumSen); 
    
   onewire_write(125,NumSen); 
   onewire_write(-55,NumSen); //this should be done for proper working of DS18B20 
   onewire_write(127,NumSen); 
    
   onewire_reset(NumSen); 
   onewire_write(0xCC,NumSen); 
   onewire_write(0x48,NumSen); 
   delay_ms(15); 
} 



float ds18b20_read(int8 NumSen) 
{ 
   int8 tH,tL,Conf; 
   int8 busy=0, temp1, temp2; 
   signed int16 temp3; 
   float result; 
   
   onewire_reset(NumSen); 
   onewire_write(0xCC,NumSen); 
   onewire_write(0x44,NumSen); 
   delay_ms(200); 
   while (busy == 0) 
     busy = onewire_read(NumSen); 
   
   onewire_reset(NumSen); 
   onewire_write(0xCC,NumSen); 
   onewire_write(0xBE,NumSen); 
   temp1 = onewire_read(NumSen); 
   temp2 = onewire_read(NumSen); 
   tH=onewire_read(NumSen); 
   tL=onewire_read(NumSen); 
   Conf=onewire_read(NumSen); 
   temp3 = make16(temp2, temp1); 
   
   //result = (float) temp3 / 2.0;   // 0.5 deg C resolution 
   result = (float) temp3 / 16.0;  //0.1 deg C resolution 
   delay_ms(200); 
   return(result); 
} 
