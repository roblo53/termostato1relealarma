#include <18F26J50.h>
#device *=16 
#device ADC=10

#FUSES INTRC
#FUSES STVREN                   //Stack full/underflow will cause reset
#FUSES NOXINST                  //Extended set extension and Indexed Addressing mode disabled (Legacy mode)
#FUSES PROTECT                  //Code protected from reads
#FUSES T1DIG                    //Secondary Oscillator Source may be select regardless of T1CON.3 state
#FUSES NOLPT1OSC                //Timer1 configured for higher power operation
#FUSES FCMEN                    //Fail-safe clock monitor enabled
#FUSES IESO                     //Internal External Switch Over mode enabled
#FUSES DSWDTOSC_INT             //DSWDT uses INTRC as reference clock
#FUSES RTCOSC_T1                //RTCC uses Secondary Oscillator as reference source
#FUSES DSBOR                    //BOR enabled in Deep Sleep
#FUSES DSWDT                    //Deep Sleep Watchdog Timer enabled
#FUSES IOL1WAY                  //Allows only one reconfiguration of peripheral pins
#FUSES MSSPMSK7                 //MSSP uses 7 bit Masking mode
#FUSES WPFP                     //Write/Erase Protect Page Start/End Location, set to last page or use WPFP=x to set page
#FUSES WPEND                    //Flash pages WPFP to Configuration Words page are write/erase protected
#FUSES WPCFG                    //Configuration Words page is erase/write-protected
#FUSES WPDIS                    //All Flash memory may be erased or written

#use delay(internal=8MHz)
//#use FIXED_IO( B_outputs=PIN_B6,PIN_B5,PIN_B4,PIN_B3,PIN_B2,PIN_B1,PIN_B0 )
//#use FIXED_IO( C_outputs=PIN_C6,PIN_C5,PIN_C4,PIN_C2,PIN_C1,PIN_C0 )
#use fast_io(B)
#use fast_io(C)
#use standard_io(A)
#byte OSCCON= 0xFD3
#byte OSCTUNE= 0xF9B 
#byte ANCON0= 0xF48

#byte LATC= 0xF8B
#byte ANCON1= 0xF49
#byte UCON= 0xF65
#byte UCFG= 0xF8B
#byte TRISC= 0xF94

#define outSolenoide      PIN_B0
#define outCompresor      PIN_B1
#define ledAlarma         PIN_B4

#define inPresostato      PIN_C0
#define inBomFlow         PIN_C1

#define senTempOper       PIN_A2 //listo
#define senTempConf       PIN_A3 //listo



