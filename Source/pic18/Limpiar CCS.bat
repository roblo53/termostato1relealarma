@echo off

del *.bak /f /q /s
del *.cof /f /q /s
del *.err /f /q /s
REM                   del *.hex /f /q /s
del *.esym /f /q /s
del *.tre /f /q /s
del *.sym /f /q /s
del *.lst /f /q /s
del *.sta /f /q /s
del *.xsym /f /q /s
del *.ccspjt /f /q /s

rd /s /q __history
rd /s /q lib\__history
